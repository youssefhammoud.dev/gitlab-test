'use strict'

module.exports = {
  development: {
    db: "mongodb://root:rootpassword@mongodb_container:27017", //'mongodb://localhost/todo',
    port: process.env.PORT || 3001,
    userCookieName: 'user'
  },
  testonlocal: {
    db:  "mongodb://root:rootpassword@mongodb_container:27017", //'mongodb://localhost/todo-integration',
    port: process.env.PORT || 3001,
    userCookieName: 'user'
  },
  testongitlab: {
    db:  "mongodb://mongo:27017", //'mongodb://localhost/todo-integration',
    port: process.env.PORT || 3001,
    userCookieName: 'user'
  },
  production: {
    // db: 'mongodb://localhost/todo',
    // port: process.env.PORT || 80,
    // userCookieName: 'user'
  }
}
