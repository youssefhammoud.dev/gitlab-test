'use strict'

console.log('mongoenv');
console.log(process.argv);

// Also checks for --name and if we have a value
const nameIndex = process.argv.indexOf('--mongoenv');
let nameValue;

if (nameIndex > -1) {
  // Grabs the value after --name
  nameValue = process.argv[nameIndex + 1];
}

const env = (nameValue || 'testonlocal');

const config = require('../server/config/config')[env]

require('../server/config/mongoose')(config, env)